# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/tjgo/Desktop/DSST_Beta/dsst_tracker/dsst_tracker.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/dsst_tracker/dsst_tracker.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/dsst_tracker/dsst_tracker_track.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/dsst_tracker/dsst_tracker_track.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/dsst_tracker/dsst_tracker_update.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/dsst_tracker/dsst_tracker_update.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/fHog/fhog.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/fHog/fhog.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/get_sample.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/get_sample.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/main.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/main.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/tools/tools.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/tools/tools.cpp.o"
  "/home/tjgo/Desktop/DSST_Beta/tools/tools_cuda.cpp" "/home/tjgo/Desktop/DSST_Beta/cmake-build-debug/CMakeFiles/dsst_beta.dir/tools/tools_cuda.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include/opencv4"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
