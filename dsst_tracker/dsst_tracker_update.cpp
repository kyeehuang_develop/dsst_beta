#include "dsst_tracker.h"

void dsst_tracker::scale_update(cv::Mat img)
{
    cv::Mat scale_fHog;
    cv::Mat scale_fHog_fft;
    cv::Mat scale_fft_temp[2];
    float scaleFactorsNew[nSCALE];
    for (int i = 0; i < this->nScale; ++i)
        scaleFactorsNew[i] = this->current_scale_factor*this->scaleFactors[i];

    scale_fHog = get_scale_sample(img,this->pos,this->model_sz,this->nScale,scaleFactorsNew,this->cell_sz,this->cos_window_s);
    cv::dft(scale_fHog,scale_fHog_fft,cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT); /// one dimension dft in rows
    //gpu::dft(scale_fHog,scale_fHog_fft,scale_fHog.size(),cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT);
    //cv::split(scale_fHog_fft,scale_fft_temp);
    GPU_split(scale_fHog_fft,scale_fft_temp,2);

    this->As_real = (1-learning_rate)*this->As_real + learning_rate*(this->gaussian_s_fft[0].mul(scale_fft_temp[0]) + this->gaussian_s_fft[1].mul(scale_fft_temp[1]));
    this->As_imag = (1-learning_rate)*this->As_imag + learning_rate*(this->gaussian_s_fft[0].mul(scale_fft_temp[1]) - this->gaussian_s_fft[1].mul(scale_fft_temp[0]));
    this->Bs      = (1-learning_rate)*this->Bs + learning_rate*(scale_fft_temp[0].mul(scale_fft_temp[0]) + scale_fft_temp[1].mul(scale_fft_temp[1]));

}

void dsst_tracker::trans_update(cv::Mat img)
{
    cv::Mat trans_sample_fHog = get_translation_sample(img,this->pos,this->model_sz,this->cell_sz,this->current_scale_factor,this->cos_window_t);
    ///trans_sample = gradient(trans_sample);

    cv::Mat fHog_temp[28];
    //cv::split(trans_sample_fHog,fHog_temp);
    GPU_split(trans_sample_fHog,fHog_temp,28);

    cv::Mat fHog_fft[2];
    cv::Mat fft_temp;

    for (int j = 0; j < 28; ++j) {

        cv::dft(fHog_temp[j],fft_temp,cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
        //gpu::dft(fHog_temp[j],fft_temp,fHog_temp[j].size(),cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
        GPU_split(fft_temp,fHog_fft,2);

        cv::Mat A_real_new = this->gaussian_t_fft[_REAL_].mul(fHog_fft[_REAL_]) + this->gaussian_t_fft[_IMAG_].mul(fHog_fft[_IMAG_]);
        cv::Mat A_imag_new = this->gaussian_t_fft[_REAL_].mul(fHog_fft[_IMAG_]) - this->gaussian_t_fft[_IMAG_].mul(fHog_fft[_REAL_]);
        cv::Mat B_real_new = fHog_fft[_REAL_].mul(fHog_fft[_REAL_]) + fHog_fft[_IMAG_].mul(fHog_fft[_IMAG_]);

        this->At_real[j] = (1-this->learning_rate)*this->At_real[j] + this->learning_rate*A_real_new;
        this->At_imag[j] = (1-this->learning_rate)*this->At_imag[j] + this->learning_rate*A_imag_new;
        this->Bt[j] = (1-this->learning_rate)*this->Bt[j] + this->learning_rate*B_real_new;
    }
}

