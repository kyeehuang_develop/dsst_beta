//
// Created by PotatoMaxwell on 2020/2/2.
//
#include "dsst_tracker.h"

void dsst_tracker::trans_track(cv::Mat img)
{
    cv::Mat trans_gaussian;
    cv::Mat scale_gaussian;
    cv::Mat trans_sample;
    cv::Mat scale_sample;

    cv::Mat cos_window;
    cv::Mat fft_temp;

    /// Get translation sample feature which is a 28-channels fHog feature map
    cv::Mat trans_sample_fHog;
    trans_sample_fHog = get_translation_sample(img,this->pos,this->model_sz,this->cell_sz,this->current_scale_factor,this->cos_window_t);
    cv::Mat fHog_temp[28];

    GPU_split(trans_sample_fHog,fHog_temp,28);

    cv::Mat fHog_fft[2];
    cv::Mat result_fft[2];

    /// calculation of the filter through 28 channels in Fourier's domain
    cv::Mat num_real = cv::Mat::zeros(this->fHog_sz[_Y_],this->fHog_sz[_X_],CV_32FC1);
    cv::Mat num_imag = cv::Mat::zeros(this->fHog_sz[_Y_],this->fHog_sz[_X_],CV_32FC1);
    cv::Mat den      = cv::Mat::zeros(this->fHog_sz[_Y_],this->fHog_sz[_X_],CV_32FC1);
    for (int j = 0; j < 28; ++j) {
        cv::dft(fHog_temp[j],fft_temp,cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
        //gpu::dft(fHog_temp[j],fft_temp,fHog_temp[j].size(),cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
        GPU_split(fft_temp,fHog_fft,2);

        num_real += (At_real[j].mul(fHog_fft[_REAL_]) + At_imag[j].mul(fHog_fft[_IMAG_]));
        num_imag += (At_real[j].mul(fHog_fft[_IMAG_]) - At_imag[j].mul(fHog_fft[_REAL_]));
        den += (Bt[j]);

    }

    result_fft[_REAL_] = num_real/(den+lambda_t);
    result_fft[_IMAG_] = num_imag/(den+lambda_t);

    /// inverse Fourier Transform to show the match points
    cv::Mat result;

    //cv::merge(result_fft,2,result);
    GPU_merge(result_fft,2,result);

    cv::Mat out;
    cv::idft(result,out,cv::DFT_REAL_OUTPUT);
    cv::normalize(out, out,1,0,cv::NORM_MINMAX);
    //gpu::normalize(out, out,1,0,4,-1,cv::NORM_MINMAX);
    cv::imshow("calcu",out);

    /// search the biggest response peak
    double MaxVal = 0;
    cv::Point MaxLoc;
    //cv::minMaxLoc(out,NULL,&MaxVal,NULL,&MaxLoc);
    gpu::minMaxLoc(out,NULL,&MaxVal,NULL,&MaxLoc);

    /// update the position information
    this->velocity[_X_] = (MaxLoc.x-(int)out.cols/2)*this->cell_sz + this->cell_sz/2*((MaxLoc.y-(int)out.rows/2)>0)- this->cell_sz/2*((MaxLoc.y-(int)out.rows/2)<0);
    this->velocity[_Y_] = (MaxLoc.y-(int)out.rows/2)*this->cell_sz + this->cell_sz/2*((MaxLoc.y-(int)out.rows/2)>0)- this->cell_sz/2*((MaxLoc.y-(int)out.rows/2)<0);

    this->pos[_Y_] += this->velocity[_Y_];
    this->pos[_X_] += this->velocity[_X_];

}

void dsst_tracker::scale_track(cv::Mat img)
{
    /// get scale feature map
    cv::Mat scale_fHog;
    scale_fHog = get_scale_sample(img,this->pos,this->model_sz,this->nScale,this->scaleFactors,this->cell_sz,this->cos_window_s);

    cv::Mat scale_Hog_fft;
    cv::dft(scale_fHog,scale_Hog_fft,cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT); /// one dimension dft in rows
    //gpu::dft(scale_fHog,scale_Hog_fft,scale_fHog.size(),cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT);

    cv::Mat scale_fft_temp[2];
    GPU_split(scale_Hog_fft,scale_fft_temp,2);

    cv::Mat num_s_real = this->As_real.mul(scale_fft_temp[_REAL_]) + this->As_imag.mul(scale_fft_temp[_IMAG_]);
    cv::Mat num_s_imag = this->As_real.mul(scale_fft_temp[_IMAG_]) - this->As_imag.mul(scale_fft_temp[_REAL_]);
    cv::Mat den_s      = this->Bs + this->lambda_s;

    cv::Mat scale_fft_out[2];
    scale_fft_out[_REAL_] = num_s_real/den_s;
    scale_fft_out[_IMAG_] = num_s_imag/den_s;


    cv::Mat result,out;
    cv::reduce(scale_fft_out[_REAL_],scale_fft_out[_REAL_],0,cv::REDUCE_SUM);
    //gpu::reduce(scale_fft_out[_REAL_],scale_fft_out[_REAL_],0,cv::REDUCE_SUM);
    cv::reduce(scale_fft_out[_IMAG_],scale_fft_out[_IMAG_],0,cv::REDUCE_SUM);
    //gpu::reduce(scale_fft_out[_IMAG_],scale_fft_out[_IMAG_],0,cv::REDUCE_SUM);
    //cv::merge(scale_fft_out,2,result);
    GPU_merge(scale_fft_out,2,result);

    cv::Point MaxLoc;
    double MaxVal = 0;
    cv::idft(result,out,cv::DFT_REAL_OUTPUT);
    cv::minMaxLoc(out,NULL,&MaxVal,NULL,&MaxLoc);
    //gpu::minMaxLoc(out,NULL,&MaxVal,NULL,&MaxLoc);

    this->current_scale_factor = this->current_scale_factor*this->scaleFactors[(int)MaxLoc.x];

    this->sz[_X_] = this->model_sz[_X_];
    this->sz[_Y_] = this->model_sz[_Y_];
}
