//
// Created by PotatoMaxwell on 2020/1/27.
//

#include "dsst_tracker.h"

void dsst_tracker::init(cv::Mat ori, int pos[2], int sz[2])
{
    cv::Mat trans_gaussian;
    cv::Mat scale_gaussian;
    cv::Mat trans_sample;
    cv::Mat scale_sample;

    cv::Mat cos_window;

    float scaleFactor = 1.02;
    this->nScale = nSCALE;

    for (int i = 0; i < nScale; ++i)
        this->scaleFactors[i] = pow(scaleFactor,i-(int)((this->nScale)/2));

    this->pos[0] = pos[0];
    this->pos[1] = pos[1];

    this->velocity[0] = 0;
    this->velocity[1] = 0;

    this->model_sz[0] = sz[0];
    this->model_sz[1] = sz[1];

    this->current_scale_factor =1;
    this->learning_rate = 0.08;


    this->cell_sz = 8;
    this->fHog_sz[0] = (int)this->model_sz[0]/this->cell_sz;
    this->fHog_sz[1] = (int)this->model_sz[1]/this->cell_sz;


    this->lambda_t = 0.0001*cv::Mat::ones(this->fHog_sz[_Y_],this->fHog_sz[_X_],CV_32FC1);

    cv::Mat fft_temp;

    

    _cos_win_(this->cos_window_t,this->fHog_sz);
    _gaussian_(this->gaussian_t,this->fHog_sz);
    _hann_(this->cos_window_s,this->nScale);

    int scale_gaussian_sz[2];
    scale_gaussian_sz[1] = this->nScale;
    scale_gaussian_sz[0] = 1;

    cv::Mat temp_s;
    _gaussian_(temp_s,scale_gaussian_sz);
    for (int k = 0; k < this->fHog_sz[0]*this->fHog_sz[1]*28; ++k) {
        if (k>0)
            cv::vconcat(this->gaussian_s,temp_s,this->gaussian_s);
        else
            temp_s.copyTo(this->gaussian_s);
    }

//    gpu::GpuMat gaussian_s_fft_gpu;
//    gpu::GpuMat gaussian_t_fft_gpu;
//    gaussian_s_fft_gpu.upload(this->gaussian_s_fft);
//    gaussian_t_fft_gpu.upload(this->gaussian_t_fft);

    cv::dft(this->gaussian_s,fft_temp,cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT);
    //gpu::dft(this->gaussian_s, fft_temp, this->gaussian_s.size(),cv::DFT_ROWS|CV_HAL_DFT_COMPLEX_OUTPUT);
    //cv::split(fft_temp,this->gaussian_s_fft);
    GPU_split(fft_temp,this->gaussian_s_fft,2);

    cv::dft(this->gaussian_t,fft_temp,cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
    //gpu::dft(this->gaussian_t, fft_temp, this->gaussian_t.size(), cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
    //cv::split(fft_temp,this->gaussian_t_fft);
    GPU_split(fft_temp,this->gaussian_t_fft,2);

    this->lambda_s = 0.0001*cv::Mat::ones(this->gaussian_s.rows,this->gaussian_s.cols,this->gaussian_s.type());

    cv::Mat trans_sample_fHog;
    trans_sample_fHog = get_translation_sample(ori,this->pos,this->model_sz,this->cell_sz,this->current_scale_factor,this->cos_window_t);
    ///trans_sample = gradient(trans_sample);

    cv::Mat fHog_temp[28];

    //cv::split(trans_sample_fHog,fHog_temp);
    GPU_split(trans_sample_fHog,fHog_temp,28);

    cv::Mat fHog_fft[2];
    for (int j = 0; j < 28; ++j) {

        cv::dft(fHog_temp[j],fft_temp,cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);
        //gpu::dft(fHog_temp[j],fft_temp, fHog_temp[j].size(),cv::DFT_SCALE|cv::DFT_COMPLEX_OUTPUT);

        //cv::split(fft_temp,fHog_fft);
        GPU_split(fft_temp,fHog_fft,2);

        this->At_real[j] = this->gaussian_t_fft[_REAL_].mul(fHog_fft[_REAL_]) + this->gaussian_t_fft[_IMAG_].mul(fHog_fft[_IMAG_]);
        this->At_imag[j] = this->gaussian_t_fft[_REAL_].mul(fHog_fft[_IMAG_]) - this->gaussian_t_fft[_IMAG_].mul(fHog_fft[_REAL_]);

        this->Bt[j] = fHog_fft[_REAL_].mul(fHog_fft[_REAL_]) + fHog_fft[_IMAG_].mul(fHog_fft[_IMAG_]);

    }

    cv::Mat scale_fHog;
    scale_fHog = get_scale_sample(ori,this->pos,this->model_sz,this->nScale,this->scaleFactors,this->cell_sz,this->cos_window_s);

    cv::Mat scale_Hog_fft;
    cv::dft(scale_fHog,scale_Hog_fft,cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT); /// one dimension dft in rows
    //gpu::dft(scale_fHog,scale_Hog_fft, scale_fHog.size(),cv::DFT_ROWS|cv::DFT_COMPLEX_OUTPUT);

    cv::Mat scale_fft_temp[2];
    GPU_split(scale_Hog_fft,scale_fft_temp,2);

    this->As_real = this->gaussian_s_fft[0].mul(scale_fft_temp[0]) + this->gaussian_s_fft[1].mul(scale_fft_temp[1]);
    this->As_imag = this->gaussian_s_fft[0].mul(scale_fft_temp[1]) - this->gaussian_s_fft[1].mul(scale_fft_temp[0]);
    this->Bs      = scale_fft_temp[0].mul(scale_fft_temp[0]) + scale_fft_temp[1].mul(scale_fft_temp[1]);




}

void dsst_tracker::track(cv::Mat ori)
{
    this->trans_track(ori);
    this->trans_update(ori);
    this->scale_track(ori);
    this->scale_update(ori);
}



