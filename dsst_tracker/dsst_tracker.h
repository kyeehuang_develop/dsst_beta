//
// Created by PotatoMaxwell on 2020/1/27.
//

#ifndef FHOG_DSST_TRACKER_H
#define FHOG_DSST_TRACKER_H

#define nSCALE 11
#include "../fHog/fhog.h"
#include <opencv2/cudaarithm.hpp>
#include "../tools/tools_cuda.h"
namespace  gpu = cv::cuda;

class dsst_tracker{
public:
    void init(cv::Mat img,int pos[2],int sz[2]);
    void track(cv::Mat img);
    void scale_update(cv::Mat img);
    void trans_update(cv::Mat img);
    void trans_track(cv::Mat img);
    void scale_track(cv::Mat img);

    float current_scale_factor;
    int pos[2];
    int velocity[2];
    int sz[2];
private:
    cv::Mat At_real[28];
    cv::Mat At_imag[28];
    cv::Mat Bt[28];
    cv::Mat lambda_t;

    cv::Mat As_real;
    cv::Mat As_imag;
    cv::Mat Bs;
    cv::Mat lambda_s;

    cv::Mat gaussian_s;
    cv::Mat gaussian_t;
    cv::Mat cos_window_t;
    cv::Mat cos_window_s;

    cv::Mat gaussian_t_fft[2];
    cv::Mat gaussian_s_fft[2];


    int model_sz[2];
    int cell_sz;
    float scaleFactors[nSCALE];
    int fHog_sz[2];
    int nScale;
    float learning_rate;
};

cv::Mat get_scale_sample(cv::Mat img,int pos[2],int model_sz[2],int nScale,float scaleFactors[nSCALE],int cell_sz,cv::Mat cos_window);
cv::Mat get_translation_sample(cv::Mat img,int pos[2],int model_sz[2],int cell_sz,float scale_factor,cv::Mat window);
#endif //FHOG_DSST_TRACKER_H
