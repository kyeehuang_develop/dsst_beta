//
// Created by tjgo on 2/27/20.
//

#include "tools_cuda.h"
#include <math.h>
#include <iostream>

void GPU_split(const cv::_InputArray &src, cv::Mat* mat, int channels_num)
{
    gpu::GpuMat Gpumat_zeros;
    Gpumat_zeros.upload(mat[0]);
    gpu::GpuMat channels_gpu[channels_num];
    for (int i = 0; i < channels_num; i++)
    {
        channels_gpu[i] = Gpumat_zeros;
    }
    gpu::split(src,channels_gpu);
    for (int i = 0; i < channels_num; i++) {
        channels_gpu[i].download(mat[i]);
    }
}

void GPU_merge(const cv::Mat* src, int channels_num, const cv::_OutputArray &dst)
{
    gpu::GpuMat channels_gpu[channels_num];
    for (int i = 0; i < channels_num; i++)
    {
        channels_gpu[i].upload(src[i]);
    }
    gpu::merge(channels_gpu, channels_num, dst);
}
