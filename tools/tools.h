//
// Created by PotatoMaxwell on 2020/1/23.
//

#ifndef FHOG_TOOLS_H
#define FHOG_TOOLS_H

#define SIGMA 0.85;
#define PI 3.1415926f
#define _X_ 1
#define _Y_ 0
#define _REAL_ 0
#define _IMAG_ 1

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/cudaarithm.hpp"
namespace  gpu = cv::cuda;

void _hann_(cv::Mat &hann , int sz);
void _cos_win_(cv::Mat &cos_window,int sz[2]);
void _gaussian_(cv::Mat &gaussian,int sz[2]);
char _mul_(cv::Mat A,cv::Mat B,cv::Mat &C);
char _div_(cv::Mat A,cv::Mat B,cv::Mat &C);
void _y_mat_(cv::Mat & y_mat);
void _x_mat_(cv::Mat & x_mat);
void _mat_geo_center_(cv::Mat img,cv::Mat y_mat,cv::Mat x_mat,int pos[2]);
void mark_target(int pos[2],int sz[2],cv::Mat &img,int color,float scaleFactor);

#endif //FHOG_TOOLS_H
