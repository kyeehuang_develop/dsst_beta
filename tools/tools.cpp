//
// Created by PotatoMaxwell on 2020/1/23.
//

#include "tools.h"
#include <math.h>
#include <iostream>


void _hann_(cv::Mat &hann , int sz)
{
    cv::Mat temp(1,sz,CV_32F);
    for (int i = 0; i < temp.rows; ++i)
        for (int j = 0; j < temp.cols; ++j)
        {
            float val = (float)0.5*(1-cos(2*PI*j/temp.cols));
            temp.at<float>(i,j) = (float)val;
            //std::cout<<temp.at<float>(j,i)<<"  "<<val<<std::endl;
                }
    temp.copyTo(hann);
    //std::cout<<hann.at<float>(1,(int)hann.cols/2)<<std::endl;

}

/** using to generate cosine windows **/
using namespace cv;
void _cos_win_(cv::Mat &cos_window,int sz[2])
{
    cv::Mat hann1,hann2;
    _hann_(hann1,sz[1]);
    _hann_(hann2,sz[0]);
    cv::transpose(hann2,hann2);
    //gpu::transpose(hann1,hann2);
    cv::Mat temp;
    temp = hann2*hann1;

    cv::Mat cos_temp[28];
    for (int i = 0; i < 28; ++i)
        temp.copyTo(cos_temp[i]);

    //gpu::GpuMat cos_temp_gpu[28];
    //cos_temp_gpu.upload(cos_temp);
    cv::merge(cos_temp,28,cos_window);
    //gpu::merge(cos_temp_gpu,28,cos_window);
}

void _gaussian_(cv::Mat &gaussian,int sz[2])
{
    float sigma_factor = 0.10;
    int rows = sz[0];
    int cols = sz[1];
    float sigma_x = sqrt(cols*cols)*sigma_factor;
    float sigma_y = sqrt(rows*rows)*sigma_factor;
    sigma_x = sigma_x * sigma_x;
    sigma_y = sigma_y * sigma_y;


    cv::Mat gau(rows,cols,CV_32F);
    float temp;
    float *ptr = (float*)gau.data;
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j)
        {
            int index = i*cols+j;
            temp = exp(-0.5* (((j-cols/2)*(j-cols/2))/sigma_x + ((i-rows/2)*(i-rows/2))/sigma_y) );
            ptr[index] = temp;
            //temp = ptr[index];

        }
    gau.copyTo(gaussian);

}


char _mul_(cv::Mat A,cv::Mat B,cv::Mat &C)
{
    if ((A.rows!=B.rows) || (A.cols!=B.cols))
        return 0;
    B.copyTo(C);
    float * ptr_B = (float*)B.data;
    unsigned char  * ptr_A = (unsigned char*)A.data;
    float  * ptr_C = (float*)C.data;

    for (int i = 0; i < C.rows; ++i)
    {
        for (int j = 0; j < C.cols; ++j)
        {
            ptr_C[i*C.cols+j]=(float)((ptr_B[i*C.cols+j])*ptr_A[i*C.cols+j]/255);
            //std::cout<<"cos:"<<ptr_B[i*C.cols+j]<<"  data:"<<ptr_A[i*C.cols+j]<<"  re:"<<ptr_C[i*C.cols+j]<<std::endl;
        }
    }
    return 1;

}

char _div_(cv::Mat A,cv::Mat B,cv::Mat &C)
{
    if ((A.rows!=B.rows) || (A.cols!=B.cols))
        return 0;
    B.copyTo(C);
    float * ptr_B = (float*)B.data;
    float  * ptr_A = (float*)A.data;
    float  * ptr_C = (float*)C.data;

    for (int i = 0; i < C.rows; ++i)
    {
        for (int j = 0; j < C.cols; ++j)
        {
            ptr_C[i*C.cols+j]=(float)((ptr_A[i*C.cols+j])/ptr_B[i*C.cols+j]);
            //std::cout<<"cos:"<<ptr_B[i*C.cols+j]<<"  data:"<<ptr_A[i*C.cols+j]<<"  re:"<<ptr_C[i*C.cols+j]<<std::endl;
        }
    }
    return 1;

}

void _x_mat_(cv::Mat & x_mat)
{
    float * ptr = (float*)x_mat.data;
    int cols = x_mat.cols;
    for (int i = 0; i < x_mat.rows; ++i)
        for (int j = 0; j < x_mat.cols; ++j)
            ptr[i*cols+j] = j;
}

void _y_mat_(cv::Mat & y_mat)
{
    float * ptr = (float*)y_mat.data;
    int cols = y_mat.cols;
    for (int i = 0; i < y_mat.rows; ++i)
        for (int j = 0; j < y_mat.cols; ++j)
            ptr[i*cols+j] = i;
}

void _mat_geo_center_(cv::Mat img,cv::Mat y_mat,cv::Mat x_mat,int pos[2])
{
    Scalar sum_img = sum(img);
    Scalar x = sum(img.mul(x_mat));
    Scalar y = sum(img.mul(y_mat));

    pos[_Y_] = y.val[0]/sum_img.val[0];
    pos[_X_] = x.val[0]/sum_img.val[0];
}

void mark_target(int pos[2],int sz[2],cv::Mat &img,int color,float scaleFactor)
{


    int tl_y = (1+pos[_Y_]-(int)sz[_Y_]*scaleFactor/2)/1;
    int tl_x = (1+pos[_X_]-(int)sz[_X_]*scaleFactor/2)/1;
    int br_y = (pos[_Y_]+(int)sz[_Y_]*scaleFactor/2)/1;
    int br_x = (pos[_X_]+(int)sz[_X_]*scaleFactor/2)/1;

    tl_x = tl_x*(tl_x>=1) + (tl_x<1);
    tl_y = tl_y*(tl_y>=1) + (tl_y<1);

    br_x = br_x*(br_x<=img.cols)+(br_x>img.cols)*img.cols;
    br_y = br_y*(br_y<=img.rows)+(br_y>img.rows)*img.rows;

    cv::Point patch_tl(tl_x,tl_y);
    cv::Point patch_br(br_x,br_y);

    if (color == 1)
    cv::rectangle(img,patch_tl,patch_br,cv::Scalar(255,0,0),2,cv::LINE_8);

    else if (color == 2)
        cv::rectangle(img,patch_tl,patch_br,cv::Scalar(0,255,0),2,cv::LINE_8);

    else
        cv::rectangle(img,patch_tl,patch_br,cv::Scalar(0,0,255),2,cv::LINE_8);



}


