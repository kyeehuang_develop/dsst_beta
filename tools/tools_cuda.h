//
// Created by tjgo on 2/27/20.
//

#ifndef DSST_BETA_TOOLS_CUDA_H
#define DSST_BETA_TOOLS_CUDA_H

#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/cudaarithm.hpp"
namespace  gpu = cv::cuda;

void GPU_split(const cv::_InputArray &src, cv::Mat* mat, int channels_num);
void GPU_merge(const cv::Mat* src, int channels_num, const cv::_OutputArray &dst);

#endif //DSST_BETA_TOOLS_CUDA_H
