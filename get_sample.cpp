//
// Created by PotatoMaxwell on 2020/1/28.
//

#include "fHog/fhog.h"
#include <math.h>


/// pos[posy posx] sz[rows cols]  x--cols--width y--rows--height
cv::Mat get_translation_sample(cv::Mat img,int pos[2],int model_sz[2],int cell_sz,float scale_factor,cv::Mat window)
{
    int patch_sz[2] = {0};
    patch_sz[_X_] = ((model_sz[_X_]*(model_sz[_X_]>=2) + (model_sz[_X_]<2))*scale_factor)/1; /// make sure the size is bigger than 2
    patch_sz[_Y_] = ((model_sz[_Y_]*(model_sz[_Y_]>=2) + (model_sz[_Y_]<2))*scale_factor)/1;

    int tl_y = (1+pos[_Y_]-(int)patch_sz[_Y_]/2)/1;
    int tl_x = (1+pos[_X_]-(int)patch_sz[_X_]/2)/1;
    int br_y = (pos[_Y_]+(int)patch_sz[_Y_]/2)/1;
    int br_x = (pos[_X_]+(int)patch_sz[_X_]/2)/1;

    tl_x = tl_x*(tl_x>=1) + (tl_x<1);
    tl_y = tl_y*(tl_y>=1) + (tl_y<1);

    br_x = br_x*(br_x<=img.cols)+(br_x>img.cols)*img.cols;
    br_y = br_y*(br_y<=img.rows)+(br_y>img.rows)*img.rows;

    cv::Point patch_tl(tl_x,tl_y);
    cv::Point patch_br(br_x,br_y);
    cv::Rect patch_rect(patch_tl,patch_br);
    cv::Mat patch = img(patch_rect);
    cv::resize(patch,patch,cv::Size(model_sz[_X_],model_sz[_Y_]));
    //gpu::resize(patch,patch,cv::Size(model_sz[_X_],model_sz[_Y_]));

    cv::Mat sample;
    //patch = gradient(patch);
    sample = fhog_compute(patch,cell_sz);
    sample = sample.mul(window);

    return sample;

}


cv::Mat get_scale_sample(cv::Mat img,int pos[2],int model_sz[2],int nScale,float scaleFactors[33],int cell_sz,cv::Mat cos_window)
{
    /// window to get fHog feature
    cv::Mat sample;
    cv::Mat one_window = cv::Mat::ones((int)model_sz[0]/cell_sz,(int)model_sz[1]/cell_sz,CV_32FC1);
    cv::Mat cos_temp[28];
    for (int i = 0; i < 28; ++i)
        one_window.copyTo(cos_temp[i]);

//    gpu::GpuMat cos_temp_gpu(cos_temp);
    //cv::merge(cos_temp,28,one_window);
//    gpu::merge(cos_temp_gpu,28,one_window);
    GPU_merge(cos_temp,28,one_window);

    cv::Mat fft_temp;
    for (int i = 0; i < nScale; ++i)
    {
        cv::Mat fHog;
        float val = (float)0.5*(1-cos(2*PI*i/nScale));

        /// fHog get
        fHog = get_translation_sample(img,pos,model_sz,cell_sz,scaleFactors[i],one_window);
        /// reshape to vector
        fHog = val*fHog.reshape(1,fHog.rows*fHog.cols*28); /// now become a 1 channel 1*rows*cols*channels Mat
        if (i>0)
            cv::hconcat(sample,fHog,sample);
        else
            fHog.copyTo(sample);
    }

    return sample;

}