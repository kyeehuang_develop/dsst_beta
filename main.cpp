#include <iostream>
#include "dsst_tracker/dsst_tracker.h"
#include "fHog/fhog.h"
#include "tools/tools_cuda.h"
using namespace cv;


int main()
{
    VideoCapture cam("/home/tjgo/Videos/output_fast.avi");
    dsst_tracker tracker_R,tracker_G,tracker_B;

    Mat ori,img;
    cam>>ori;
    //cvtColor(ori,ori,COLOR_BGR2GRAY);
    //ori.convertTo(img, CV_32F);
    Mat channels[3];
    GPU_split(ori,channels,3);

    channels[2].convertTo(channels[2],CV_32F);
    //channels[1].convertTo(channels[1],CV_32F);
    //channels[2].convertTo(channels[2],CV_32F);

    int sz[2] = {100,180};
    int pos[2] = {220,290};


    tracker_B.init(channels[2],pos,sz);
    //tracker_G.init(channels[1],pos,sz);
    //tracker_R.init(channels[2],pos,sz);


    mark_target(tracker_B.pos,tracker_B.sz,ori,1,tracker_B.current_scale_factor);
    //mark_target(tracker_G.pos,tracker_G.sz,ori,2);
    //mark_target(tracker_R.pos,tracker_R.sz,ori,3);

    while(!ori.empty())
    {
        cam>>ori;
        //cvtColor(ori,ori,COLOR_BGR2GRAY);
        //ori.convertTo(img, CV_32F);
        Mat channels[3];
        GPU_split(ori,channels,3);

        channels[2].convertTo(channels[2],CV_32F);
        //channels[1].convertTo(channels[1],CV_32F);
        //channels[2].convertTo(channels[2],CV_32F);

        tracker_B.track(channels[2]);
        //tracker_G.track(channels[1]);
        //tracker_R.track(channels[2]);


        mark_target(tracker_B.pos,tracker_B.sz,ori,3,tracker_B.current_scale_factor);
        std::cout<<"scale:"<<tracker_B.current_scale_factor<<std::endl;
        //mark_target(tracker_G.pos,tracker_G.sz,ori,2);
        //mark_target(tracker_R.pos,tracker_R.sz,ori,3);

        imshow("mark",ori);
        waitKey(10);
    }

    return 0;
}