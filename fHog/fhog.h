//
// Created by PotatoMaxwell on 2020/1/28.
//

#ifndef FHOG_FHOG_H
#define FHOG_FHOG_H

#include "../tools/tools.h"
#include "../tools/tools_cuda.h"
#include "opencv2/cudaarithm.hpp"
namespace gpu = cv::cuda;

cv::Mat fhog_compute(cv::Mat img,int cell_sz);
cv::Mat gradient(cv::Mat img);

#endif //FHOG_FHOG_H
