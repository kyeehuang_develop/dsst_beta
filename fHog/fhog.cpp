//
// Created by PotatoMaxwell on 2020/1/28.
//

#include "fhog.h"

/// pass out a 2 channels matrix with channel[0] represent of the magnitude of gradient , while channel[1] represent of the orientation in [0,255];
cv::Mat gradient(cv::Mat img)
{
//    cv::imshow("ori",img);
    cv::Mat temp[2];
    /// matrix shift
    cv::Mat down_shift ,up_shift , left_shift , right_shift;
    cv::vconcat(img.row(0)*2,img.rowRange(0,img.rows-1),down_shift);
    cv::hconcat(img.col(0)*2,img.colRange(0,img.cols-1),right_shift);
    cv::hconcat(img.colRange(1,img.cols),img.col(img.cols-1)*2,left_shift);
    cv::vconcat(img.rowRange(1,img.rows),img.row(img.rows-1)*2,up_shift);

    /// horizontal and vertical gradient calculate
    cv::Mat horizon = down_shift - up_shift;
    cv::Mat vertical = right_shift - left_shift;

    /// channel[0], magnitude of the gradient
    //cv::sqrt(horizon.mul(horizon) + vertical.mul(vertical),temp[0]);
    gpu::sqrt(horizon.mul(horizon) + vertical.mul(vertical),temp[0]);

    //horizon.copyTo(temp[0]);
    //vertical.copyTo(temp[1]);


    temp[0].copyTo(temp[1]);
    /// channel[1], orientation of the gradient
    float * ptr = (float*) temp[1].data;
    float * ptr_h = (float*) horizon.data;
    float * ptr_v = (float*) vertical.data;

    int p = 18;
    for (int i = 0; i < horizon.rows; ++i)
    {
        for (int j = 0; j < horizon.cols; ++j)
        {
            int index = i*horizon.cols + j;
            float k = ptr_v[index]/((ptr_h[index]==0)*0.001+ptr_h[index]);
            float angle = atan(k)+((ptr_v[index])<0)*PI;
            angle = (angle<0)*(angle+PI*2) + (angle>=PI*2)*(angle-PI*2) +angle*(angle>=0)*(angle<PI*2);
            ptr[index] = (int)(p*angle/(2*PI));
            //std::cout<<" "<<ptr[index];
        }
    //std::cout<<std::endl;
    }

    /// merge channels
    cv::Mat out;
    //cv::merge(temp,2,out)
    GPU_merge(temp,2,out);

    //temp[0].copyTo(out);
    return out;
}

cv::Mat fhog_compute(cv::Mat img,int cell_sz)
{

    cv::Mat grad = gradient(img);
    int block_x = grad.cols/cell_sz;
    int block_y = grad.rows/cell_sz;
    /// init
    cv::Mat fHog_temp = cv::Mat::zeros(block_y,block_x,CV_32FC(28));
    cv::Mat lambda_temp[28];
    for (int m = 0; m < 28; ++m)
        lambda_temp[m] = 0.00001*cv::Mat::ones(block_y,block_x,CV_32FC1);

    cv::Mat lambda;
    //cv::merge(lambda_temp,28,lambda);
    GPU_merge(lambda_temp,28,lambda);

    cv::Mat temp[2];
    //cv::split(grad,temp);
    GPU_split(grad,temp,2);

    cv::Mat fHog_channels[28];
    //cv::split(fHog_temp,fHog_channels);
    GPU_split(fHog_temp,fHog_channels,28);

    float * ptr_orients = (float*)temp[1].data;
    float * ptr_mag = (float*)temp[0].data;


    for (int i = 0; i < fHog_temp.rows; ++i) {
        for (int k = 0; k < cell_sz; ++k) {
            for (int j = 0; j < fHog_temp.cols; ++j) {
                for (int l = 0; l < cell_sz; ++l) {
                    int index = (i*cell_sz+k)*grad.cols+(j*cell_sz+l);
                    int orient = (int)ptr_orients[index];
                    fHog_channels[orient+1].at<float>(i,j) += ptr_mag[index];
                    fHog_channels[(orient>=9)*(orient-9) + (orient<9)*orient + 18 +1].at<float>(i,j) += ptr_mag[index];
                    //std::cout<<" "<<ptr_mag[index];
                }
            }
            //std::cout<<std::endl;
        }
    }



    /// merge
    /// this->img.copyTo(fHog_channels[0]);
    //cv::merge(fHog_channels,28,fHog_temp);
    GPU_merge(fHog_channels,28,fHog_temp);

    cv::Mat up_shift ;
    cv::vconcat(fHog_temp.rowRange(1,fHog_temp.rows),fHog_temp.row(fHog_temp.rows-1),up_shift);

    cv::Mat down_shift;
    cv::vconcat(fHog_temp.row(0),fHog_temp.rowRange(0,fHog_temp.rows-1),down_shift);

    cv::Mat left_shift;
    cv::hconcat(fHog_temp.colRange(1,fHog_temp.cols),fHog_temp.col(fHog_temp.cols-1),left_shift);

    cv::Mat right_shift;
    cv::hconcat(fHog_temp.col(0),fHog_temp.colRange(0,fHog_temp.cols-1),right_shift);

    cv::Mat down_right_shift;
    cv::vconcat(right_shift.row(0),right_shift.rowRange(0,right_shift.rows-1),down_right_shift);

    cv::Mat down_left_shift;
    cv::vconcat(left_shift.row(0),left_shift.rowRange(0,left_shift.rows-1),down_left_shift);

    cv::Mat up_right_shift;
    cv::vconcat(right_shift.rowRange(1,right_shift.rows),right_shift.row(right_shift.rows-1),up_right_shift);

    cv::Mat up_left_shift;
    cv::vconcat(left_shift.rowRange(1,left_shift.rows),left_shift.row(left_shift.rows-1),up_left_shift);

    cv::Mat N[4];

    cv::sqrt(down_shift.mul(down_shift) + right_shift.mul(right_shift) + down_right_shift.mul(down_right_shift) + fHog_temp.mul(fHog_temp)+lambda,N[0]);
    cv::sqrt(down_shift.mul(down_shift) + left_shift.mul(right_shift) + down_left_shift.mul(down_left_shift) + fHog_temp.mul(fHog_temp)+lambda,N[1]);
    cv::sqrt(up_shift.mul(down_shift) + right_shift.mul(right_shift) + up_right_shift.mul(up_right_shift) + fHog_temp.mul(fHog_temp)+lambda,N[2]);
    cv::sqrt(up_shift.mul(down_shift) + left_shift.mul(right_shift) + up_left_shift.mul(up_left_shift) + fHog_temp.mul(fHog_temp)+lambda,N[3]);
    //gpu::sqrt(down_shift.mul(down_shift) + right_shift.mul(right_shift) + down_right_shift.mul(down_right_shift) + fHog_temp.mul(fHog_temp)+lambda,N[0]);
    //gpu::sqrt(down_shift.mul(down_shift) + left_shift.mul(right_shift) + down_left_shift.mul(down_left_shift) + fHog_temp.mul(fHog_temp)+lambda,N[1]);
    //gpu::sqrt(up_shift.mul(down_shift) + right_shift.mul(right_shift) + up_right_shift.mul(up_right_shift) + fHog_temp.mul(fHog_temp)+lambda,N[2]);
    //gpu::sqrt(up_shift.mul(down_shift) + left_shift.mul(right_shift) + up_left_shift.mul(up_left_shift) + fHog_temp.mul(fHog_temp)+lambda,N[3]);

    cv::Mat out = cv::Mat::zeros(block_y,block_x,CV_32FC(28));
    for (int i = 0; i < 4; ++i)
    {
        N[i] = fHog_temp/N[i];
        out += N[i];
    }

    //cv::split(out,fHog_channels);
    GPU_split(out,fHog_channels,28);

    cv::Mat _img_;
    cv::resize(img,_img_,cv::Size(block_x,block_y));
    //gpu::resize(img,_img_,cv::Size(block_x,block_y));
    _img_.copyTo(fHog_channels[0]);
    //cv::merge(fHog_channels,28,out);
    GPU_merge(fHog_channels,28,out);

    /// cv::resize(fHog,fHog,cv::Size(grad.cols,grad.rows));

    return out;
}

